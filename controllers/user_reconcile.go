/***************************************************
 ** @Desc : This file for 用户对账
 ** @Time : 2019.04.18 10:35 
 ** @Author : Joker
 ** @File : user_reconcile
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.18 10:35
 ** @Software: GoLand
****************************************************/
package controllers

import (
	"recharge/models"
	"recharge/utils"
	"strconv"
	"strings"
)

type UserReconcile struct {
	KeepSession
}

var reconDao = models.UserReconcileDao{}

// 展示对账界面
// @router /user/reconcile_ui/ [get]
func (the *UserReconcile) ShowUserReconcileUI() {
	userName := the.GetSession("userName")
	if userName != nil {
		the.Data["userName"] = userName.(string)
	}

	right := the.GetSession("FilterRight")
	if right != nil {
		the.Data["right"] = right.(int)
	}

	the.TplName = "reconciliation.html"
}

// 对账列表查询分页
// @router /user/reconcile_list/?:params [get]
func (the *UserReconcile) QueryAndListPage() {
	//分页参数
	page, _ := strconv.Atoi(the.GetString("page"))
	limit, _ := strconv.Atoi(the.GetString("limit"))
	if limit == 0 {
		limit = 15
	}

	//查询参数
	in := make(map[string]interface{})
	inT := models.UserReconcileDao{}
	inToday := models.UserReconcileDao{}
	uName := strings.TrimSpace(the.GetString("uName"))
	start := strings.TrimSpace(the.GetString("uStart"))
	end := strings.TrimSpace(the.GetString("uEnd"))
	uType := the.GetString("uType") //类型

	if start != "" {
		//in["edit_time__gte"] = start
		inT.EditTime = start
	}
	if end != "" {
		//in["edit_time__lte"] = end
		inT.EditTimeEnd = end
	}

	in["user_name__icontains"] = uName
	if uType != "" {
		in["authority"] = uType[len(uType)-1:]
	}

	//计算分页数
	count := userMdl.SelectUserPageCount(in)
	totalPage := count / limit // 计算总页数
	c := count%limit
	if c != 0 { // 不满一页的数据按一页计算
		totalPage++
	}

	//数据获取
	var list []models.UserInfo
	if page <= totalPage {
		list = userMdl.SelectUserListPage(in, limit, (page-1)*limit)
		inT.Status = utils.S
		inToday.Status = utils.S
		inToday.EditTime = globalMethod.GetNowTimeV3() + " 00:01:05"
		inToday.EditTimeEnd = globalMethod.GetNowTime()
		for i := 0; i < len(list); i++ {
			list[i].Authority += 100
			userId := strconv.Itoa(list[i].Id)
			inT.UserId = userId
			inToday.UserId = userId

			// 计算转账
			ct := reconDao.SelectCountSuccessTransfer(inT)
			list[i].TransferByRecord = ct.Total

			//计算 代付
			cp := reconDao.SelectCountSuccessPay(inT)
			list[i].PayByRecord = cp.Total

			//计算 成功充值金额
			cr := reconDao.SelectCountSuccessRecharge(inT)
			list[i].RechargeByRecord = cr.Total

			// 计算手动加减款
			cs := PMinusMdl.SelectOneCustomPlusMinusByUserId(list[i].Id)
			var add, ded, pro float64
			for _, v := range cs {
				add += v.Addition
				ded += v.Deduction
				pro += v.HandingFee
			}
			list[i].HandleAddition = add
			list[i].HandleDeduction = ded

			//算利润
			// 算出的利润可能比实际利润高也可能低：
			// 此利润没有减去成本，也没有加上手动减款的利润
			// 不确定上游给的单笔充值/代付成本是多少，但是我方给下游的单笔成本肯定是不小于上游给的
			rate := (list[i].RechargeRate-1) * 0.001
			profitT := ct.Total * rate
			profitP := float64(cp.Length) * (list[i].PayFee-1)
			profitR := cr.Total * rate
			list[i].ProfitByRecord = profitP + profitT + profitR + pro

			//算余额
			list[i].BalanceByRecord = ct.Total + cr.Total + add - cp.Total - list[i].ProfitByRecord - ded

			// 计算今天转账
			transferToday := reconDao.SelectCountSuccessTransfer(inToday)
			list[i].TransferByRecordToday = transferToday.Total

			//计算今天 代付
			payToday := reconDao.SelectCountSuccessPay(inToday)
			list[i].PayByRecordToday = payToday.Total

			//计算今天 成功充值金额
			rechargeToday := reconDao.SelectCountSuccessRecharge(inToday)
			list[i].RechargeByRecordToday = rechargeToday.Total

			//算今天利润
			profitTToday := transferToday.Total * rate
			profitPToday := float64(payToday.Length) * (list[i].PayFee-1)
			profitRToday := rechargeToday.Total * rate
			list[i].ProfitByRecordToday = profitPToday + profitTToday + profitRToday
		}
	}

	//数据回显
	out := make(map[string]interface{})
	out["limit"] = limit //分页数据
	out["page"] = page
	out["totalPage"] = totalPage
	out["root"] = list //显示数据

	the.Data["json"] = out
	the.ServeJSON()
	the.StopRun()
}
