/***************************************************
 ** @Desc : This file for 用户模型控制
 ** @Time : 2019.03.30 16:58
 ** @Author : Joker
 ** @File : user_info_act
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.03.30 16:58
 ** @Software: GoLand
****************************************************/
package controllers

import (
	"github.com/dchest/captcha"
	"recharge/models"
	"recharge/sys"
	"recharge/utils"
	"strings"
)

type UserLogin struct {
	KeepSession
}

var globalMethod = utils.GlobalMethod{}
var userMdl = models.UserInfo{}

//加载登录页面
// @router /login [get,post]
func (the *UserLogin) ShowLoginUI() {
	capt := struct {
		CaptchaId string
	}{
		captcha.NewLen(4),
	}
	the.Data["CaptchaId"] = capt.CaptchaId
	the.TplName = "login.html"
}

// 验证输入的验证码
// @router /verifyCaptcha.py/:value/:chaId [get]
func (the *UserLogin) VerifyCaptcha() {
	captchaId := the.Ctx.Input.Param(":chaId")
	captchaValue := the.Ctx.Input.Param(":value")

	verify := captcha.VerifyString(captchaId, captchaValue)
	if verify {
		the.Data["json"] = globalMethod.JsonFormat(utils.SUCCESS_FLAG, "", "", "")
	} else {
		the.Data["json"] = globalMethod.JsonFormat(utils.FAILED_FLAG, "", "验证码不匹配!", "")
	}
	the.ServeJSON()
	the.StopRun()
}

// 重绘验证码
// @router /flushCaptcha.py/ [get]
func (the *UserLogin) FlushCaptcha() {
	capt := struct {
		CaptchaId string
	}{
		captcha.NewLen(4),
	}
	the.Data["json"] = globalMethod.JsonFormat(utils.SUCCESS_FLAG, capt.CaptchaId, "验证码不匹配!", "")
	the.ServeJSON()
	the.StopRun()
}

// 登录发送短信验证码
// @router /sms.do/?:params [post]
func (the *UserLogin) SmsPhoneCode() {
	verifyFlag := the.GetString("verify_flag")
	userName := strings.Trim(the.GetString("userName"), " ")
	password := the.GetString("Password")

	var msg = ""
	var flag = utils.FAILED_FLAG

	msg, verify, u := verificationLogin(verifyFlag, userName, password)
	if verify {
		loginCookie := the.Ctx.GetCookie("login_cookie")
		if loginCookie == "" {
			the.SetSession("login_cookie", "")
		}

		loginCookieS := the.GetSession("login_cookie")
		codeCookie := ""
		if loginCookieS != nil {
			codeCookie = loginCookieS.(string)
		}
		if loginCookie != "" || strings.Compare(loginCookie, codeCookie) != 0 {
			msg = "请在2分钟后送登录验证码！"
		} else {
			code := globalMethod.RandomIntOfString(6)
			isSuccess := sms.SendSmsCode(u.Mobile, code)
			if !isSuccess {
				msg = "验证码发送失败，请联系管理员核实预留手机号是否正确！"
			} else {
				flag = utils.SUCCESS_FLAG
				the.SetSession("login_code", code)

				cookie := globalMethod.RandomString(6)
				the.SetSession("login_cookie", cookie)
				the.Ctx.SetCookie("login_cookie", cookie, 2*60)

				msg = "验证码发送成功，且2分钟内有效！"
			}
		}
	}

	the.Data["json"] = globalMethod.JsonFormat(flag, "", msg, "")
	the.ServeJSON()
	the.StopRun()
}

// 用户登录
// @router /login.py/?:params [post]
func (the *UserLogin) UserLogin() {
	//verifyFlag := the.GetString("verify_flag")
	userName := strings.Trim(the.GetString("userName"), " ")
	password := the.GetString("Password")
	//phoneCode := strings.Trim(the.GetString("phoneCode"), " ")

	var msg = ""
	var url = "/"
	var flag = utils.FAILED_FLAG

	msg, verify, u := verificationLogin("", userName, password)
	if verify {
		//短信验证码
		//loginCode := the.GetSession("login_code")
		//if loginCode == nil {
		//	msg = "请点击发送登录验证码！"
		//} else {
			//if strings.Compare(loginCode.(string), phoneCode) != 0 {
			//	msg = "短信验证码输入错误！"
			//} else {
				//验证密码
				pwdMd5 := globalMethod.LoginPasswordSolt(password, u.Salt)
				if strings.Compare(pwdMd5, u.LoginPwd) != 0 {
					msg = "密码输入错误!"
				} else {
					the.SetSession("FilterUsers", u.LoginName)
					the.SetSession("FilterRight", u.Authority)
					the.SetSession("userName", u.UserName)
					the.SetSession("userId", u.Id)

					url = "/merchant/"
					flag = utils.SUCCESS_FLAG
					sys.LogInfo("【", u.UserName, "】用户登录成功! ")

					cookie := the.Ctx.GetCookie("login_cookie") //两分钟失效
					if cookie == "" {
						the.SetSession("login_code", "")
						the.SetSession("login_cookie", "")
					}
				}
			//}
		//}
	}

	the.Data["json"] = globalMethod.JsonFormat(flag, "", msg, url)
	the.ServeJSON()
	the.StopRun()
}

/* *
 * @Description: 验证登录参数
 * @Author: Joker
 * @Date: 2019.04.04 11:06
 * @Param: code: 验证码; name; 用户名; pwd: 密码
 * @return: msg: 错误信息; flag: 参数是否有误; u: 用户信息
 **/
func verificationLogin(code, name, pwd string) (msg string, flag bool, u models.UserInfo) {
	if name == "" || pwd == "" {
		return "登录账号或密码不能为空！", false, u
	}

	/*if strings.Compare("1", code) != 0 {
		return "验证码不正确!", false, u
	}*/

	isExit := userMdl.UserIsExit(name)
	if !isExit {
		return "用户不存在!", false, u
	}

	u = userMdl.SelectOneUser(name)
	if u.Status == 1 {
		return "用户已经冻结，请联系管理员解冻！", false, u
	}

	return "", true, u
}

// 退出登录
//@router /loginOut.py
func (the *UserLogin) LoginOut() {
	the.DelSession("FilterUsers")
	the.DelSession("FilterRight")
	the.DelSession("userName")
	the.DelSession("login_code")
	the.Ctx.SetCookie("login_cookie", "")
	the.SetSession("login_cookie", "")

	the.Data["json"] = globalMethod.JsonFormat(utils.FAILED_FLAG, "", "", "/")
	the.ServeJSON()
	the.StopRun()
}

// 用户检查
//@router /IsRight
func (the *UserLogin) IsRightUser() {
	the.DelSession("FilterUsers")
	the.DelSession("FilterRight")
	the.DelSession("userName")
	the.DelSession("login_code")
	the.Ctx.SetCookie("login_cookie", "")
	the.SetSession("login_cookie", "")

	the.Ctx.Redirect(302, "/")
}
